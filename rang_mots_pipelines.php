<?php

/**
 * Pipelines pour plugin Rang mots
 *
 * @plugin     Rang sur les mots
 * @copyright  2021
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Rang_mots\Pipelines
 */

 /**
 * Ajoute la gestion du glisser-déposer pour gérer l'ordre des mots-clés
 *
 * @pipeline affiche_milieu
 * @param array $flux
 * @return array
 */
function rang_mots_affiche_milieu($flux) {
	if(strpos($flux['data'], 'formulaire_editer_liens-mots')) {
		$flux['data'] .= recuperer_fond(
			'prive/squelettes/inclure/deplacer_mots');
	}

	return $flux;
}