<?php

/**
 * Action ordonnant un lien sur une table de liens
 *
 * @plugin     Rang mots
 * @copyright  2021
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Rang_mots\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/ordonner_liens_documents');

function action_ordonner_liens_mots_dist() {
	action_ordonner_liens_dist();
}
