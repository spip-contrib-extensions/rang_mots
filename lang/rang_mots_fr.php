<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// D
	'deplacer_element' => 'Déplacer cet élément',

	// L
	'lien_tout_desordonner' => 'Réinitialiser l’ordre',
	'lien_ordonner' => 'Ordonner les mots',

	// R
	'rang_mots_titre' => 'Rang sur les mots',

);
